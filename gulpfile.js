var gulp = require('gulp'),
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  prefixer = require('gulp-autoprefixer'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  webpack = require('webpack'),
  webpackStream = require('webpack-stream'),
  browserSync = require('browser-sync').create();

gulp.task('scripts', function () {
  return gulp.src('./src/js/app.js')
    .pipe(webpackStream({
      output: {
        filename: 'app.js',
      },
      module: {
        rules: [
          {
            test: /\.(js)$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader',
            query: {
              presets: ['env']
            }
          }
        ]
      },
      externals: {
        jquery: 'jQuery'
      }
    }))
    .pipe(gulp.dest('./public/'))
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./public/'));
});

gulp.task('styles', function () {
  return gulp.src('./src/scss/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(prefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./public/'))
    .pipe(browserSync.stream());
});


gulp.task('serve', ['styles'], function () {
  browserSync.init({
    server: "./public"
  });

  gulp.watch('./src/scss/**/*.scss', ['styles']);
  gulp.watch('./src/js/**/*.js', ['scripts']);

  gulp.watch(['./public/*.html', './public/app.js']).on('change', browserSync.reload);
});

gulp.task('default', ['styles', 'scripts', 'serve']);
