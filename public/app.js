/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var update = function update(fieldType, value) {
  $('[data-profile-field=' + fieldType + ']').text(value);
  $('[data-profile-field-input=' + fieldType + ']').val(value);
};

exports.default = function (fieldType, value) {
  // I'm not happy with that but that logic a bit special
  if (fieldType === 'lastName') return;

  if (fieldType === 'firstName') {
    var lastName = $('.editProfileHandheld__text-lastName').val();
    update('fullName', value + ' ' + lastName);
  } else if (fieldType === 'fullName') {
    var fullNameArray = value.split(' ');

    update('firstName', fullNameArray[0]);
    update('lastName', fullNameArray[1]);
    update('fullName', value);
  } else {
    update(fieldType, value);
  }
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(3);

__webpack_require__(4);

__webpack_require__(5);

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _jquery = __webpack_require__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _updateProfileField = __webpack_require__(1);

var _updateProfileField2 = _interopRequireDefault(_updateProfileField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _jquery2.default)(document).on('click', '.profileField__btn', function (event) {
  var $btn = (0, _jquery2.default)(event.currentTarget);
  var $scope = $btn.closest('.profileField');
  var $popup = (0, _jquery2.default)('.popupEditField');
  var title = $scope.data('placeholder');
  var inputVal = (0, _jquery2.default)('.profileField__value', $scope).text();

  (0, _jquery2.default)('.popupEditField__title').text(title);
  (0, _jquery2.default)('.popupEditField__text').val(inputVal);
  $popup.appendTo($scope).show();
});

(0, _jquery2.default)(document).on('click', '.popupEditField__btn-no', function () {
  (0, _jquery2.default)('.popupEditField').hide();
});

(0, _jquery2.default)(document).on('click', '.popupEditField__btn-yes', function (event) {
  var $scope = (0, _jquery2.default)(event.currentTarget).closest('.profileField');
  var text = (0, _jquery2.default)('.popupEditField__text', $scope).val();
  var fieldType = (0, _jquery2.default)('[data-profile-field]', $scope).data('profile-field');

  (0, _updateProfileField2.default)(fieldType, text);
  (0, _jquery2.default)('.popupEditField').hide();
});

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _jquery = __webpack_require__(0);

var _jquery2 = _interopRequireDefault(_jquery);

var _updateProfileField = __webpack_require__(1);

var _updateProfileField2 = _interopRequireDefault(_updateProfileField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var updateVisibility = function updateVisibility() {
  //Toggle visibility of necessary for editing classes
  (0, _jquery2.default)('.editProfileHandheld, .profileHandheld, .pageContent__editHandheldProfile, .pageContent__controls').toggleClass('hidden');
};

(0, _jquery2.default)(document).on('click', '.pageContent__editHandheldProfile, .pageContent__actionBtn-no', function () {
  updateVisibility();
});

(0, _jquery2.default)(document).on('click', '.pageContent__actionBtn-yes', function () {
  (0, _jquery2.default)('.editProfileHandheld__text').each(function () {
    var text = (0, _jquery2.default)(this).val();
    var fieldType = (0, _jquery2.default)(this).data('profile-field-input');

    (0, _updateProfileField2.default)(fieldType, text);
  });

  updateVisibility();
});

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _jquery = __webpack_require__(0);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _jquery2.default)(document).on('click', '.navigation__link', function (event) {
  event.preventDefault();
  var linkItem = '.navigation__item';
  var linkItemActive = 'navigation__item-active';
  var $link = (0, _jquery2.default)(event.target);
  var tab = $link.attr('href');

  (0, _jquery2.default)(linkItem).removeClass(linkItemActive);
  $link.closest(linkItem).addClass(linkItemActive);

  (0, _jquery2.default)('.pageContent').addClass('hidden');
  (0, _jquery2.default)('.pageContent[data-tab=' + tab + ']').removeClass('hidden');
});

/***/ })
/******/ ]);