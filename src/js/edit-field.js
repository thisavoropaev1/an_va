import $ from 'jquery';
import updateProfileField from './update-profile-field.js';

$(document).on('click', '.profileField__btn', (event) => {
  let $btn = $(event.currentTarget);
  let $scope = $btn.closest('.profileField');
  let $popup = $('.popupEditField');
  let title = $scope.data('placeholder');
  let inputVal = $('.profileField__value', $scope).text();

  $('.popupEditField__title').text(title);
  $('.popupEditField__text').val(inputVal);
  $popup.appendTo($scope).show();
});

$(document).on('click', '.popupEditField__btn-no', () => {
  $('.popupEditField').hide();
});

$(document).on('click', '.popupEditField__btn-yes', (event) => {
  let $scope = $(event.currentTarget).closest('.profileField');
  let text = $('.popupEditField__text', $scope).val();
  let fieldType = $('[data-profile-field]', $scope).data('profile-field');

  updateProfileField(fieldType, text);
  $('.popupEditField').hide();
});
