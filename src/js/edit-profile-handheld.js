import $ from 'jquery';
import updateProfileField from './update-profile-field.js';

let updateVisibility = () => {
  //Toggle visibility of necessary for editing classes
  $('.editProfileHandheld, .profileHandheld, .pageContent__editHandheldProfile, .pageContent__controls').toggleClass('hidden');
};

$(document).on('click', '.pageContent__editHandheldProfile, .pageContent__actionBtn-no', () => {
  updateVisibility();
});

$(document).on('click', '.pageContent__actionBtn-yes', () => {
  $('.editProfileHandheld__text').each(function() {
    let text = $(this).val();
    let fieldType = $(this).data('profile-field-input');

    updateProfileField(fieldType, text);
  });

  updateVisibility();
});
