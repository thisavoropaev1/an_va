import $ from 'jquery';

$(document).on('click', '.navigation__link', (event) => {
  event.preventDefault();
  const linkItem = '.navigation__item';
  const linkItemActive = 'navigation__item-active';
  const $link = $(event.target);
  const tab = $link.attr('href');

  $(linkItem).removeClass(linkItemActive);
  $link.closest(linkItem).addClass(linkItemActive);

  $('.pageContent').addClass('hidden');
  $(`.pageContent[data-tab=${tab}]`).removeClass('hidden');
});
