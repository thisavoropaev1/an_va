let update = (fieldType, value) => {
  $(`[data-profile-field=${fieldType}]`).text(value);
  $(`[data-profile-field-input=${fieldType}]`).val(value);
};

export default (fieldType, value) => {
  // I'm not happy with that but that logic a bit special
  if (fieldType === 'lastName') return;

  if (fieldType === 'firstName') {
    const lastName = $('.editProfileHandheld__text-lastName').val();
    update('fullName', `${value} ${lastName}`);
  } else if (fieldType === 'fullName') {
    const fullNameArray = value.split(' ');

    update('firstName', fullNameArray[0]);
    update('lastName', fullNameArray[1]);
    update('fullName', value);
  } else {
    update(fieldType, value);
  }
};
